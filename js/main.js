/**
 * Author: Pete Januarius, Nexgen Codecamp Pty Ltd
 * Asset Credits:
 *  
 * 
 * Game Development Order
 * 1. index.html, load Phaser
 * 2. Phaser Configuration & Arcade Physics
 * 3. Preload, Create, Update methods
 * 4. (Pre)Loading Assets
 * 5. Creating Game Objects
 * 6. Adding Sprites
 * 7. Keyboard controls & player movement
 * 8. Setting World boundaries and collision events
 * 9. Adding a timer with events
 * 10. Adding game state with levels
 * 11. Collision detection to wall
 * 12. Add text to the screen
 * 13. Incrementing to next level
 * 14. Resetting the timer and level text
 * 15. Stop timer on gameover
 * 16. Restart the game
 * 17. Add fruit randomly spawning
 * 18. Add overlap calculation
 * 19. Destroy fruit when collide with player
 * 20. Increase speed of player when hit
 * 21. Exit keyboard controls if game not in play (disable keys)
 * 22. On start, position & set the player going right, left, up or down
 * 23. Add Game Over text - then start game.
 * 24. Add score
 * 25. Add fruit data to separate js file
 * 26. Add sound for collecting fruit and player die
 * 
 *
 * Additional Techniques Learnt
 * 1. Callback functions
 * 2. Events
 * 3. Using new to create objects
 */

const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    parent: "game-container",
    pixelArt: true,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

let gameState = {
    started: false,
    gameOver: false,
    level: 1,
    timeLeft: 60,
    score: 0
}

const game = new Phaser.Game(config);

let PLAYER_VELOCITY = 200;
let cursorKeys;
let gameTimer;
let walls;
let timeLeftText;
let levelText;
let clickToStartText;
let gameOverText;
let scoreText;
let fruits = [];
let sndFruitHit;
let sndDie;

// Game Objects
var playerSprite;

function preload() {
    // Load background
    this.load.image('sky', 'images/space1.png')

    // Load sprites
    this.load.image('player', 'images/32x32.png')
    this.load.image('wall-h', 'images/wall-horizontal.png')
    this.load.image('wall-v', 'images/wall-vertical.png')
    this.load.spritesheet('fruits', 'images/fruitnveg32wh37.png', {
        frameHeight: 32,
        frameWidth: 32
    })

    // Load audio
    this.load.audio('hitFruit', './snd/levelup.wav');
    this.load.audio('die', './snd/player-die.mp3');
}

function create() {
    // Static images
    let sky = this.add.image(400, 300, 'sky');
    sky.setInteractive();
    sky.setName("sky");
    this.add.image(400, 12, 'wall-h');
    this.add.image(400, 588, 'wall-h');
    this.add.image(12, 300, 'wall-v');
    this.add.image(788, 300, 'wall-v');

    // Sounds
    sndFruitHit = this.sound.add('hitFruit');
    sndDie = this.sound.add('die');

    // Setup text
    timeLeftText = this.add.text(32, 32, 'TIMELEFT: ' + gameState.timeLeft, {
        fontSize: '16px',
        fill: '#fff'
    });
    levelText = this.add.text(156, 32, 'LVL: ' + gameState.level, {
        fontSize: '16px',
        fill: '#fff'
    });
    clickToStartText = this.add.text(330, 250, 'Click to Start', {
        fontSize: '16px',
        fill: '#fff'
    });
    scoreText = this.add.text(650, 32, 'SCORE: ' + gameState.score, {
        fontSize: '16px',
        fill: '#fff'
    });
    gameOverText = this.add.text(250, 250, '', {
        fontSize: '36px',
        fill: '#FF0000',
        fontWeight: 'bold'
    });

    // Create the walls
    walls = this.physics.add.staticGroup();
    walls.create(400, 12, 'wall-h');
    walls.create(400, 588, 'wall-h');
    walls.create(12, 300, 'wall-v');
    walls.create(788, 300, 'wall-v');

    // Sprites
    playerSprite = this.physics.add.sprite(400, 330, 'player');
    playerSprite.setBounce(0, 0);
    playerSprite.setName("Player");
    playerSprite.on("endOfLevel", nextLevel, this);

    // Collide with walls
    this.physics.add.collider(playerSprite, walls, hitWall, null, this);

    // Add keyboard input/controls
    cursorKeys = this.input.keyboard.createCursorKeys();

    timerConfig = {
        delay: 1000,
        callback: timerEvent,
        callbackScope: this,
        loop: true,
        paused: true
    }
    gameTimer = this.time.addEvent(timerConfig);

    // Fruit timer
    fruitTimerCfg = {
        delay: 5000,
        callback: fruitEvent,
        callbackScope: this,
        loop: true,
        paused: true
    }
    fruitTimer = this.time.addEvent(fruitTimerCfg);

    this.input.on('gameobjectdown', objectClicked);
}

function objectClicked(pointer, gameObject) {
    console.log("object clicked", pointer, gameObject);

    // start game
    if (!gameState.started && gameObject.name === "sky") {
        // Start the game
        clickToStartText.setVisible(false);
        gameTimer.paused = false;
        fruitTimer.paused = false;
        gameState.started = true;

        // Give the player an initial velocity
        playerSprite.setVelocityX(PLAYER_VELOCITY);
        playerSprite.setVelocityY(0);
    }
}

function update(time, delta) {
    movePlayer();
    scoreText.setText("SCORE: " + gameState.score);
}

function nextLevel() {
    console.log("Next Level...");
    // Reset timer
    gameState.timeLeft = 60;
    // Update Level number
    gameState.level++;
}

function hitWall(player, wall) {
    console.log("%cG.A.M.E.O.V.E.R", "color:#FFFF00;background-color:#000;font-size:36px", arguments, this);
    // Play sound
    sndDie.play();

    // handle game over
    gameOver(this);
}

function hitFruit(player, fruit) {
    console.log("You hit a fruit!");
    // Play sound
    sndFruitHit.play();

    // Destroy the fruit
    fruit.disableBody(true, true)

    // Update the score
    gameState.score += fruitInfo[fruit.id].points

    // Increase current velocity
    PLAYER_VELOCITY *= 1.25;
}

function gameOver(context) {
    // Set game status
    gameState.gameOver = true;

    // Set game over text
    gameOverText.setText('G.A.M.E  O.V.E.R');

    // Stop the timers
    gameTimer.reset(timerConfig);
    fruitTimer.reset(fruitTimerCfg);

    // Stop the player
    playerSprite.setVelocityX(0);
    playerSprite.setVelocityY(0);

    // Tint the player
    playerSprite.setTint(0x990000, 0x990000, 0xFF0000, 0xFF0000);

    // Remove all of the fruit objects if left
    if (fruits.length > 0) {
        fruits.forEach(item => item.sprite.destroy())
        fruits = [];
    }

    // Restart the game
    window.setTimeout(() => {
        restart();
    }, 3000);
}

function restart() {
    // Reset game state
    gameState.level = 1;
    gameState.gameOver = false;
    gameState.started = false;
    gameState.timeLeft = 60;
    gameState.score = 0;
    PLAYER_VELOCITY = 200;

    // Setup text
    gameOverText.setText("");
    timeLeftText.setText('TIMELEFT: ' + gameState.timeLeft);
    levelText.setText('LVL: ' + gameState.level);
    clickToStartText.setVisible(true);
    scoreText.setText('SCORE: ' + gameState.score);

    // Reset position of player
    playerSprite.setPosition(100, 450)

    // Clear tint
    playerSprite.clearTint();
}

function timerEvent() {
    // Update time left
    gameState.timeLeft--;
    timeLeftText.setText('TIMELEFT: ' + gameState.timeLeft);
    levelText.setText('LVL: ' + gameState.level);

    if (gameState.timeLeft === 0) {
        // trigger next level
        playerSprite.emit("endOfLevel")
    }
}

function fruitEvent() {
    // Make sure there is no overlap
    let x, y;
    do {
        // Fetch x,y for new fruit and refetch if there is an overlap
        x = Phaser.Math.Between(32, 768);
        y = Phaser.Math.Between(32, 568);
        console.log("overlap")

    } while (isOverlap(x, y))

    let fruitIndex = Math.floor(Math.random() * 37)
    let fruitSprite = this.physics.add.sprite(x, y, 'fruits', fruitIndex);
    fruitSprite.id = fruitIndex;
    // Collide with fruits
    this.physics.add.overlap(playerSprite, fruitSprite, hitFruit, null, this)
    fruits.push({
        idx: fruitIndex,
        sprite: fruitSprite
    });

    // Will need to check for overlap and probably the total max every level
}

function isOverlap(x, y) {
    let overlap = false;
    fruits.forEach(item => {
        if ((item.sprite.x > x && item.sprite.x < (x + 64)) && (item.sprite.y > y && item.sprite.y < (y + 64))) {
            overlap = true;
        }
    })
    return overlap;
}

function movePlayer() {
    // Only relevant if game is in play
    if (!gameState.started || gameState.gameOver) return;

    var isUpDown = cursorKeys.up.isDown;
    var isDownDown = cursorKeys.down.isDown;
    var isLeftDown = cursorKeys.left.isDown;
    var isRightDown = cursorKeys.right.isDown;
    var isSpaceDown = cursorKeys.space.isDown;
    var isShiftDown = cursorKeys.shift.isDown;

    if (isUpDown) {
        playerSprite.setVelocityY(-PLAYER_VELOCITY);
        playerSprite.setVelocityX(0);
    } else if (isDownDown) {
        playerSprite.setVelocityY(PLAYER_VELOCITY);
        playerSprite.setVelocityX(0);
    } else if (isRightDown) {
        playerSprite.setVelocityX(PLAYER_VELOCITY);
        playerSprite.setVelocityY(0);
    } else if (isLeftDown) {
        playerSprite.setVelocityX(-PLAYER_VELOCITY);
        playerSprite.setVelocityY(0);
    }
}