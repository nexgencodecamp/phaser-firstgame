const fruitInfo = [{
        id: 1,
        points: 10
    },
    {
        id: 2,
        points: 10
    },
    {
        id: 3,
        points: 10
    },
    {
        id: 4,
        points: 10
    },
    {
        id: 5,
        points: 10
    },
    {
        id: 6,
        points: 10
    },
    {
        id: 7,
        points: 10
    },
    {
        id: 8,
        points: 10
    },
    {
        id: 9,
        points: 10
    },
    {
        id: 10,
        points: 10
    },
    {
        id: 11,
        points: 20
    },
    {
        id: 12,
        points: 20
    },
    {
        id: 13,
        points: 20
    },
    {
        id: 14,
        points: 20
    },
    {
        id: 15,
        points: 20
    },
    {
        id: 16,
        points: 20
    },
    {
        id: 17,
        points: 20
    },
    {
        id: 18,
        points: 20
    },
    {
        id: 19,
        points: 20
    },
    {
        id: 20,
        points: 20
    },
    {
        id: 21,
        points: 30
    },
    {
        id: 22,
        points: 30
    },
    {
        id: 23,
        points: 30
    },
    {
        id: 24,
        points: 30
    },
    {
        id: 25,
        points: 30
    },
    {
        id: 26,
        points: 30
    },
    {
        id: 27,
        points: 30
    },
    {
        id: 28,
        points: 30
    },
    {
        id: 29,
        points: 30
    },
    {
        id: 30,
        points: 30
    },
    {
        id: 31,
        points: 50
    },
    {
        id: 32,
        points: 100
    },
    {
        id: 33,
        points: 150
    },
    {
        id: 34,
        points: 200
    },
    {
        id: 35,
        points: 300
    },
    {
        id: 36,
        points: 400
    },
    {
        id: 37,
        points: 500
    }
]